# BoilerplateProject<!-- omit in toc -->

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Django-restframeworkのAPIテンプレート

- [Prerequisites](#prerequisites)
- [How to](#how-to)
  - [Setup](#setup)
    - [`.env`ファイルを復元](#envファイルを復元)
  - [Iac（AWS RDS PostgresSQL）](#iacaws-rds-postgressql)
  - [Iac (AWS ECS Fargate)](#iac-aws-ecs-fargate)
  - [Develop](#develop)
  - [Connect AIP](#connect-aip)
  - [Allow host](#allow-host)
  - [ヘルスチェック](#ヘルスチェック)
  - [APIドキュメント](#apiドキュメント)
  - [Docker container終了](#docker-container終了)
  - [LICENSE](#license)
  - [Contributors](#contributors)

## Prerequisites

- Python (version 3.10-bullseye)
  - Django (version >=4.0,<5.0)
    - Django Rest Framework

## How to

### Setup

#### `.env`ファイルを復元

```.env
SECRET_KEY=*********
DEBUG=*********

DATABASE_NAME=*********
DATABASE_USER=*********
DATABASE_PASSWORD=*********
DATABASE_HOST=*********
DATABASE_PORT=*********
```

(public repositoryの場合)

1. 管理者にgpg公開鍵を教えてください.
2. `brew install git-secret`で環境セットアップ.
3. `git secret reveal`で.envファイルを復元.
4. 暗号化する場合は`git secret hide`.

### Iac（AWS RDS PostgresSQL）

1. `cd Iac_db`
2. `export TF_VAR_db_username="your_username"`
3. `export TF_VAR_db_password="your_password"`
4. `terraform init`
5. `terraform validate`
6. `terraform plan`
7. `terraform apply`

### Iac (AWS ECS Fargate)

1. `cd Iac`
2. `terraform init`
3. `terraform validate`
4. `terraform plan`
5. `terraform apply`

### Develop

1. `docker compose build`
2. `docker compose up -d`

### Connect AIP

アプリケーションからAPIへのアクセス許可をホワイトリストで管理

1. CORS_ORIGIN_WHITELIST: `http://localhost:3000`

### Allow host

アプリへのアクセスは未設定（全て許可）

### ヘルスチェック

`curl -k -XGET http://0.0.0.0:8000/`

### APIドキュメント

`http://0.0.0.0:8000/docs`

### Docker container終了

1. `docker compose down`

### LICENSE

This project is licensed under the MIT License, see the [LICENSE](./LICENSE) for details.

### Contributors

- [Smart119 INC.](https://smart119.biz/)
  - [Tatsuro Yokoyama](https://gitlab.com/tatsuro-yokoyama)
