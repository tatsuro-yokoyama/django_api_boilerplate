#!/bin/bash

# マイグレーションファイルを作成
python manage.py makemigrations

# マイグレーションを実行
python manage.py migrate

# 静的ファイルを収集
python manage.py collectstatic --noinput

# gunicornでアプリケーションを実行
gunicorn BoilerplateProject.wsgi:application -w 4 -k gthread -b 0.0.0.0:8000
