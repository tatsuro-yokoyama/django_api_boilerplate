# ベースイメージを指定
FROM --platform=linux/amd64 python:3.10-slim as base

# 作業ディレクトリを設定
WORKDIR /app

# ビルドステージ
FROM base as builder

# 必要なパッケージをインストール
RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential gcc && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Pythonライブラリをインストール
COPY requirements.txt .
RUN python -m venv /opt/venv && \
    . /opt/venv/bin/activate && \
    pip install --no-cache-dir -r requirements.txt

# アプリケーションステージ
FROM base

# 環境変数を設定
ENV PATH="/opt/venv/bin:$PATH"

# ビルドステージからファイルをコピー
COPY --from=builder /opt/venv /opt/venv
COPY . .

# entrypoint.shを実行可能にする
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# ポートを公開
EXPOSE 8000

# エントリーポイントを設定
ENTRYPOINT ["/entrypoint.sh"]
