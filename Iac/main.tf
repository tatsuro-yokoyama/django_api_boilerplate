provider "aws" {
  region = "ap-northeast-1"
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["test-vpc"]
  }
}

resource "aws_subnet" "alb_public_subnet_1a" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "ap-northeast-1a"
  tags = {
    "Name" = "django-api-boilerplate-alb-public-subnet-1a"
  }
}

resource "aws_subnet" "app_private_subnet_1a" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "ap-northeast-1a"
  tags = {
    "Name" = "django-api-boilerplate-app-private-subnet-1a"
  }
}

## ap-northeast-1c
resource "aws_subnet" "alb_public_subnet_1c" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = "10.0.5.0/24"
  availability_zone = "ap-northeast-1c"
  tags = {
    "Name" = "django-api-boilerplate-alb-public-subnet-1c"
  }
}

resource "aws_subnet" "app_private_subnet_1c" {
  vpc_id            = data.aws_vpc.selected.id
  cidr_block        = "10.0.6.0/24"
  availability_zone = "ap-northeast-1c"
  tags = {
    "Name" = "django-api-boilerplate-app-private-subnet-1c"
  }
}

resource "aws_internet_gateway" "django-api-boilerplate_internet_gateway" {
  vpc_id = data.aws_vpc.selected.id
}

### EIP ###
resource "aws_eip" "django-api-boilerplate_eip_1a" {
  domain = "vpc"
  tags = {
    "Name" = "django-api-boilerplate-eip-1a"
  }
}

resource "aws_eip" "django-api-boilerplate_eip_1c" {
  domain = "vpc"
  tags = {
    "Name" = "django-api-boilerplate-eip-1c"
  }
}

## NatGateway ###
resource "aws_nat_gateway" "django-api-boilerplate_nat_gateway_1a" {
  allocation_id = aws_eip.django-api-boilerplate_eip_1a.id
  subnet_id     = aws_subnet.alb_public_subnet_1a.id

  tags = {
    "Name" = "django-api-boilerplate-nat-gateway-1a"
  }

  depends_on = [aws_internet_gateway.django-api-boilerplate_internet_gateway]
}

resource "aws_nat_gateway" "django-api-boilerplate_nat_gateway_1c" {
  allocation_id = aws_eip.django-api-boilerplate_eip_1c.id
  subnet_id     = aws_subnet.alb_public_subnet_1c.id

  tags = {
    "Name" = "django-api-boilerplate-nat-gateway-1c"
  }

  depends_on = [aws_internet_gateway.django-api-boilerplate_internet_gateway]
}

### Route Table ###
## ALB ##
resource "aws_route_table" "alb_route_table" {
  vpc_id = data.aws_vpc.selected.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.django-api-boilerplate_internet_gateway.id
  }
  tags = {
    "Name" = "django-api-boilerplate-alb-route-table"
  }
}

resource "aws_route_table_association" "alb_table_association_1a" {
  subnet_id      = aws_subnet.alb_public_subnet_1a.id
  route_table_id = aws_route_table.alb_route_table.id
}

resource "aws_route_table_association" "alb_table_association_1c" {
  subnet_id      = aws_subnet.alb_public_subnet_1c.id
  route_table_id = aws_route_table.alb_route_table.id
}

## Application ##
# ap-northeast-1a
resource "aws_route_table" "app_route_table_1a" {
  vpc_id = data.aws_vpc.selected.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.django-api-boilerplate_nat_gateway_1a.id
  }

  tags = {
    "Name" = "django-api-boilerplate-app-route-table-1a"
  }
}

resource "aws_route_table_association" "app_route_table_association_1a" {
  subnet_id      = aws_subnet.app_private_subnet_1a.id
  route_table_id = aws_route_table.app_route_table_1a.id
}

# ap-north-east-1c
resource "aws_route_table" "app_route_table_1c" {
  vpc_id = data.aws_vpc.selected.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.django-api-boilerplate_nat_gateway_1c.id
  }

  tags = {
    "Name" = "django-api-boilerplate-app-route-table-1a"
  }
}

resource "aws_route_table_association" "app_route_table_association_1c" {
  subnet_id      = aws_subnet.app_private_subnet_1c.id
  route_table_id = aws_route_table.app_route_table_1c.id
}

### Security Group ###
## application ##
resource "aws_security_group" "app_sg" {
  name        = "app-sg"
  description = "Application Secuirty Group"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    "Name" = "django-api-boilerplate-app-sg"
  }
}

resource "aws_security_group_rule" "allow_alb_sg_inbound" {
  type                     = "ingress"
  from_port                = 8000
  to_port                  = 8000
  protocol                 = "tcp"
  security_group_id        = aws_security_group.app_sg.id
  source_security_group_id = aws_security_group.alb_sg.id
}

resource "aws_security_group_rule" "allow_every_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  security_group_id = aws_security_group.app_sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

## ALB ##
resource "aws_security_group" "alb_sg" {
  name        = "alb-sg"
  description = "ALB Secuirty Group"
  vpc_id      = data.aws_vpc.selected.id
  tags = {
    "Name" = "django-api-boilerplate-alb-sg"
  }
}

resource "aws_security_group_rule" "allow_http_inbound" {
  type              = "ingress"
  from_port         = 8000
  to_port           = 8000
  protocol          = "tcp"
  security_group_id = aws_security_group.alb_sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow_app_sg_egress" {
  type                     = "egress"
  from_port                = 8000
  to_port                  = 8000
  protocol                 = "tcp"
  security_group_id        = aws_security_group.alb_sg.id
  source_security_group_id = aws_security_group.app_sg.id
}

### ECS Cluster ###
resource "aws_ecs_cluster" "terafform_cluster" {
  name = "django-api-boilerplate_cluster"
}

resource "aws_ecs_cluster_capacity_providers" "provider" {
  cluster_name       = aws_ecs_cluster.terafform_cluster.name
  capacity_providers = ["FARGATE", "FARGATE_SPOT"]
}

### Cloud Watch Log Group ###
resource "aws_cloudwatch_log_group" "django-api-boilerplate" {
  name              = "/ecs/logs/terraform/django-api-boilerplate"
  retention_in_days = 1
}

### IAM Role ###
resource "aws_iam_role" "ecs_task_execution_role" {
  assume_role_policy = jsonencode(
    {
      Statement = [
        {
          Action = "sts:AssumeRole"
          Effect = "Allow"
          Principal = {
            Service = "ecs-tasks.amazonaws.com"
          }
          Sid = ""
        },
      ]
      Version = "2012-10-17"
    }
  )
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy",
    "arn:aws:iam::aws:policy/AmazonSESFullAccess",
  ]
  description = "Allows ECS tasks to call AWS services on your behalf."
  name        = "ecsTaskExecutionRole"
  path        = "/"
}

### ECS Task Definition
resource "aws_ecs_task_definition" "django-api-boilerplate" {
  family                   = "django-api-boilerplate"
  container_definitions    = file("./container_definition.json")
  cpu                      = 512
  memory                   = 1024
  network_mode             = "awsvpc"
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  requires_compatibilities = ["FARGATE"]
  tags = {
    "Name" = "django-api-boilerplate"
  }
}


### ECS Service ###
resource "aws_ecs_service" "django-api-boilerplate_ecs_service" {
  name = "django-api-boilerplate_ecs"
  capacity_provider_strategy {
    base              = 1
    capacity_provider = "FARGATE_SPOT"
    weight            = 1
  }
  cluster       = aws_ecs_cluster.terafform_cluster.id
  desired_count = 1
  network_configuration {
    subnets = [
      aws_subnet.app_private_subnet_1a.id,
      aws_subnet.app_private_subnet_1c.id
    ]
    security_groups  = [aws_security_group.app_sg.id]
    assign_public_ip = false
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.alb_target_group.arn
    container_name   = "django-api-boilerplate"
    container_port   = 8000
  }
  task_definition = aws_ecs_task_definition.django-api-boilerplate.arn
  tags = {
    "Name" = "django-api-boilerplate"
  }

  lifecycle {
    ignore_changes = [
      task_definition,
      desired_count,
    ]
  }
}

### ALB ###
resource "aws_alb" "alb" {
  name               = "django-api-boilerplate-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets = [
    aws_subnet.alb_public_subnet_1a.id,
    aws_subnet.alb_public_subnet_1c.id,
  ]
}

resource "aws_lb_target_group" "alb_target_group" {
  name        = "django-api-boilerplate-target-tg"
  port        = 8000
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.selected.id
  health_check {
    enabled  = true
    path     = "/"
    port     = 8000
    protocol = "HTTP"
  }
}

resource "aws_security_group_rule" "allow_http_inbound_alb" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.alb_sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_lb_listener" "alb_listner" {
  load_balancer_arn = aws_alb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}

resource "aws_security_group" "rds_sg" {
  name        = "rds-sg"
  description = "RDS Security Group"
  vpc_id      = data.aws_vpc.selected.id

  tags = {
    "Name" = "django-api-boilerplate-rds-sg"
  }
}

resource "aws_security_group_rule" "allow_app_sg_ingress" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds_sg.id
  source_security_group_id = aws_security_group.app_sg.id
}

resource "aws_security_group_rule" "allow_rds_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.rds_sg.id
}

data "aws_db_instance" "postgres" {
  db_instance_identifier = "test-postgres-db"
}

resource "aws_security_group_rule" "allow_rds_connection" {
  type                     = "ingress"
  from_port                = 5432
  to_port                  = 5432
  protocol                 = "tcp"
  security_group_id        = aws_security_group.app_sg.id
  source_security_group_id = aws_security_group.rds_sg.id
}
